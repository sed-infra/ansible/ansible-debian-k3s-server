#!/bin/bash

if [ $# -ne 1 ]; then
    echo "usage: <file.json>"
    exit 1
fi

filepath="$1"

if [ ! -f "$filepath" ]; then
    echo "File not found"
    exit 1
fi

vault_prefix="docker exec -e VAULT_ADDR=http://127.0.0.1:8200 vault"
vault_status_cmd="$vault_prefix vault status"
vault_unseal_cmd="$vault_prefix vault operator unseal"

function vault_status() {
    echo $($vault_status_cmd | head -n5 | tail -n1 | awk '{print $2}')
}

status=$(vault_status)

if [ "$status" != "true" ]; then
    echo "OK"
    exit 0
fi

sleep 5

id=0

while [ "$status" = "true" ]; do
    if [ $id -eq 5 ]; then
        break
    fi

    token="$(cat "$filepath" | jq .keys_base64 | jq -r ".[$id]")"

    $vault_unseal_cmd "$token"

    id=$(($id+1))
    status=$(vault_status)
done

echo "sealed: $status"

exit 0
